package src;

import org.jdom2.Element;

/**
 * Created by OBI on 8.11.2014.
 */
public class Default {

    public static Pool getConfig() {
        Pool attributePool = new Pool();
            attributePool.add(new Attribute<String>("DefaultTemplates",String.class, "Templates.xml"));


        return attributePool;
    }

    public static Element getTemplates(){

        Factory factory =  new Factory();

        Element result = new Element("Templates");

        result.addContent(factory.createTemplate(Pool.class));

        return result;
    }
}
