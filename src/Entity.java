package src;

import org.jdom2.Element;

/**
 * Created by obert on 4. 11. 2014.
 */
public class Entity extends EngineUnit<Entity> {

    public Pool AttributesPool;


    //region Ctors
    public Entity() {

    }
    //endregion


    //region IRead/Write
    @Override
    public void initFromNode(Element node) {
        super.initFromNode(node);
        AttributesPool.initFromNode(node.getChild(AttributesPool.getNodeName()));
    }

    @Override
    public Element writeToNode() {
        Element result = super.writeToNode();
        result.addContent(AttributesPool.writeToNode());
        return result;
    }

    @Override
    public String getNodeName() {
        return Entity.class.getName();
    }
    //endregion

    //region IInitilizable
    @Override
    public boolean preInit() {
        AttributesPool = new Pool();
        AttributesPool.setId(0);
        return true;
    }

    @Override
    public boolean init() {

        return true;
    }

    @Override
    public boolean postInit() {

        return true;
    }
    //endregion


    @Override
    public Entity memberwiseClone() {

        Entity result = new Entity();

        result.AttributesPool = AttributesPool.memberwiseClone();

        return result;
    }

    //region IDisposable
    @Override
    public boolean dispose() {
        return false;
    }

    @Override
    public boolean disposing() {
        return false;
    }
    //endregion

    //endregion
}
