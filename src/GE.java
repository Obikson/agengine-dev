package src;

/**
 * Created by OBI on 8.11.2014.
 */
public class GE {
    public static Core Core = new Core();
    public static Factory Factory = new Factory();

    public static boolean preInit() {
        Core.preInit();

        return true;
    }

    private static boolean loadConfiguration(String file) {
        XmlReader reader = new XmlReader();
        Pool config = new Pool();
        config.CanExpand = true;
        reader.parse(file, config);
        Core.setConfiguration(config.memberwiseClone());
        return true;
    }

    public static boolean init(String configurationFile) {
        loadConfiguration(configurationFile);

        Core.init();

        return true;
    }

    public static boolean postInit()
    {
        Core.postInit();
        return true;
    }


    public static <T extends EngineUnit> T templatedObject(Class<T> type, String templateName) {

        try {
            T object = type.newInstance();

            Pool<Attribute<String>> objectConfig= Core.getTemplates().getConfigs().get(templateName);
            Pool<Attribute<Integer>> objectHandles= Core.getTemplates().getHandles().get(templateName);

            Factory.loadConfiguration(type, object,objectConfig);
            Factory.loadHandles(type, object, objectHandles);

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }


        return null;
    }
}
