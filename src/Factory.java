package src;

import org.jdom2.Element;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by OBI on 5.11.2014.
 */
public class Factory {
    public Factory() {

    }

    public <T extends IReadable> T initFromNode(Element node) {
        try {
            Class<T> resultClass = (Class<T>) Class.forName(node.getName());
            T result = resultClass.newInstance();
            result.initFromNode(node);
            return result;

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }


        return null;
    }



    public <T extends IReadable> void loadFromFile(String filename, T object){
        XmlReader reader = new XmlReader();
        reader.parse(filename, object);
    }

    public <T extends IWritable> void saveToFile(String filename, T object){
        XmlWriter writer = new XmlWriter();
        try {
            writer.writeAndSave(filename, object);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    //region Handles
    public <T> Element writeHandles(Class<T> type) {

        Element result = new Element("Handles");

        for (Field field : type.getFields()) {
            Handle handle = field.getAnnotation(Handle.class);

            if (handle != null) {
                result.addContent(getHandleElement(handle.name(), handle.value()));
            }

        }
        return result;
    }

    public Pool<Attribute<Integer>> readHandles(Element parentNode) {


        Element handles = parentNode.getChild("Handles");

        List<Element> children = handles.getChildren("Handle");

        Pool<Attribute<Integer>> result = new Pool<Attribute<Integer>>(children.size());
        for (Element child : children) {
            result.add(getHandleFromElement(child), true);
        }
        return result;
    }

    private Element getHandleElement(String name, int value) {

        Element handleNode = new Element("Handle");
        handleNode.setAttribute("Name", name);
        handleNode.setAttribute("Value", Integer.toString(value));
        return handleNode;
    }

    public Attribute<Integer> getHandleFromElement(Element handleNode) {
        int id = Integer.parseInt(handleNode.getAttributeValue("Id"));
        String name = handleNode.getAttributeValue("Name");
        int value = Integer.parseInt(handleNode.getAttributeValue("Value"));

        return new Attribute<Integer>(id, name, Integer.class, value);
    }
    //endregion

    //region Configurates
    public <T> Element writeConfigurates(Class<T> type) {
        Element result = new Element("Configuration");

        Field[] fields = type.getDeclaredFields();

        for (Field field : fields) {
            Configurate configurate = field.getAnnotation(Configurate.class);

            if (configurate != null)
                result.addContent(getConfigurateElement(configurate.name(), configurate.value()));
        }

        return result;
    }

    private Element getConfigurateElement(String name, String value) {
        Element result = new Element("Configurate");
        result.setAttribute("Name", name);
        result.setAttribute("Value", value);
        return result;
    }

    private Attribute<String> getConfigurateFromElement(Element confElement) {
        String name = confElement.getAttributeValue("Name");
        String value = confElement.getAttributeValue("Value");
        return new Attribute<String>(name, String.class, value);
    }

    public Pool<Attribute<String>> readConfiguration(Element parentNode) {
        Element configNode = parentNode.getChild("Configuration");

        List<Element> children = configNode.getChildren("Configurate");
        Pool<Attribute<String>> result = new Pool<Attribute<String>>(children.size());

        for (Element child : children) {
            result.add(getConfigurateFromElement(child), true);
        }

        return result;
    }

    //endregion

    //region Templates
    public <T> Element createTemplate(Class<T> type) {
        Element result = new Element("Template");

        Template templateAnnotation = type.getAnnotation(Template.class);

        if (templateAnnotation == null)
            return result;

        result.setAttribute("Name", templateAnnotation.name());
        result.addContent(writeConfigurates(type));
        result.addContent(writeHandles(type));
        return result;
    }

    //endregion


    public <T> void loadHandles(Class<T> type, T object, Pool<Attribute<Integer>> handles) {
        Field[] fields = type.getDeclaredFields();
        for (Field field : fields) {
            Handle handle = field.getAnnotation(Handle.class);

            if (handle != null) {
                Attribute<Integer>[] items = (Attribute<Integer>[]) handles.getItems();

                for (Attribute<Integer> attrHandle : items) {
                    if (attrHandle.getName().equals(handle.name())) {
                        try {
                            field.set(object, attrHandle.getValue());
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                        break;
                    }

                }
            }
        }
    }


    public <T> void loadConfiguration(Class<T> type, T object, Pool<Attribute<String>> attributes) {

        Field[] fields = type.getDeclaredFields();

        for (Field field : fields) {
            Configurate config = field.getAnnotation(Configurate.class);
            if (config != null) {
                Attribute<String>[] items = (Attribute<String>[]) attributes.getItems();

                for (Attribute<String> attribute : items) {
                    if (attribute.getName().equals(config.name())) {
                        Class<?> fieldType = field.getType();
                        try {
                            field.set(object, attribute.parse(attribute.getValue(), fieldType));
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                        break;
                    }
                }
            }

        }
    }


}
