package src;

import org.jdom2.Element;

/**
 * Created by obert on 12. 11. 2014.
 */
public class ContentManager extends EngineUnit<ContentManager> implements IManager<Pool<EngineUnit>> {

    Pool<Pool<EngineUnit>> Items;

    @Override
    public Pool<Pool<EngineUnit>> getItems() {
        return Items;
    }

    @Override
    public void setItems(Pool<Pool<EngineUnit>> items) {
        Items = items;
    }

    @Override
    public void loadItems(String path, String filename) {
        GE.Factory.loadFromFile(path + "/" + filename, this);
    }

    @Override
    public void saveItems(String path, String filename) {
        GE.Factory.saveToFile(path + "/" + filename, this);
    }

    @Override
    public void initFromNode(Element node) {
        Items = GE.Factory.initFromNode(node);
    }

    @Override
    public Element writeToNode() {
        Element result = new Element(getNodeName());
        result.addContent(Items.writeToNode());
        return result;
    }

    @Override
    public String getNodeName() {
        return ContentManager.class.getName();
    }

    @Override
    public ContentManager memberwiseClone() {
        ContentManager manager = new ContentManager();
        manager.Items = Items.memberwiseClone();
        return manager;
    }

    @Override
    public boolean dispose() {
        return false;
    }

    @Override
    public boolean disposing() {
        return false;
    }

    @Override
    public boolean preInit() {
        Items = new Pool<Pool<EngineUnit>>();
        return false;
    }

    @Override
    public boolean init() {
        return false;
    }

    @Override
    public boolean postInit() {
        return false;
    }
}
