package src;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by obert on 6. 11. 2014.
 */
public class Utils {


    public static String getMD5Hash(String input) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("MD5");
        byte[] data = input.getBytes();
        digest.update(data);
        byte[] digested = digest.digest();

        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < digested.length; i++) {
            String hex = Integer.toHexString(0xff & digested[i]);
            if (hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }
}
