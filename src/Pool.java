package src;

import org.jdom2.Element;

import java.util.List;

/**
 * Created by obert on 7. 11. 2014.
 */
@Template(name = "Pool")
public class Pool<T extends EngineUnit> extends EngineUnit<Pool> {

    @Configurate(name = "CanExpand", value = "True")
    public boolean CanExpand = true;

    public boolean IsFull = false;

    @Configurate(name = "ExpansionAmount", value = "5")
    public int ExpansionAmount = 5;

    @Configurate(name = "Size", value = "5")
    public int Size = 0;

    public boolean isFull() {
        return Items == null || (IsFull | (Items.length == 0));
    }

    public int getSize() {
        return Size;
    }

    public void setSize(int size) {
        Size = size;
        expand(size);
    }

    public void expand(int newSize) {
        if (Items == null)
            Items = new EngineUnit[newSize];
        else {
            EngineUnit[] temp = Items;
            Items = (EngineUnit[]) (new Object[newSize]);
            System.arraycopy(Items, 0, temp, 0, temp.length);
        }


    }


    private EngineUnit<T>[] Items;

    public EngineUnit[] getItems() {
        return Items;
    }

    //region Ctors
    public Pool() {
    }


    public Pool(int size) {

        Size = size;

    }
    //endregion


    //region Functions
    public int add(EngineUnit item) {
        if (isFull() && !CanExpand)
            return -1;

        if (CanExpand) {
            int temp = Size;
            setSize(getSize() + ExpansionAmount);
            Items[temp] = item;
            item.setId(temp);
            return temp;
        }

        for (int i = 0; i < Items.length; i++) {
            if (Items[i] == null) {
                Items[i] = item;
                item.setId(i);
                return i;
            }
        }

        IsFull = true;
        return -1;
    }

    public int add(EngineUnit item, boolean overrideExisting) {
        if (!overrideExisting) {
            return add(item);
        }
        if (item.getId() >= Items.length)
            return -1;

        Items[item.getId()] = item;

        return item.getId();
    }

    public EngineUnit get(int index) {
        return Items[index];
    }

    public <T extends EngineUnit> T get(String name) {
        for (EngineUnit item : Items) {
            if (item != null && item.getName().equals(name)) {
                return (T) item;
            }
        }
        return null;
    }

    //endregion

    //region EngineUnit
    @Override
    public Pool memberwiseClone() {
        Pool result = new Pool(Items.length);
        result.preInit();
        for (int i = 0; i < Items.length; i++) {
            if (Items[i] != null)
                result.Items[i] = (EngineUnit) Items[i].memberwiseClone();
        }

        return result;
    }

    //region IDisposable
    @Override
    public boolean dispose() {
        return false;
    }

    @Override
    public boolean disposing() {
        return false;
    }
    //endregion

    //region IInitializable
    @Override
    public boolean preInit() {
        Items = new EngineUnit[getSize()];
        return false;
    }

    @Override
    public boolean init() {

        return false;
    }

    @Override
    public boolean postInit() {
        return false;
    }
    //endregion

    //region IWriteReadable
    @Override
    public void initFromNode(Element node) {
        super.initFromNode(node);

        setSize(Integer.parseInt(node.getAttributeValue("Size")));

        List<Element> children = node.getChildren();

        int counter = 0;

        for (Element child : children) {
            try {
                Class<?> type = Class.forName(child.getName());
                EngineUnit item = (EngineUnit) type.newInstance();
                item.initFromNode(child);
                Items[counter++] = item;
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public Element writeToNode() {
        Element result = super.writeToNode();
        result.setAttribute("Size", Integer.toString(Items.length));

        if (Items == null)
            return result;

        for (EngineUnit item : Items)
            if (item != null)
                result.addContent(item.writeToNode());

        return result;
    }

    @Override
    public String getNodeName() {
        return Pool.class.getName();
    }

    //endregion
    //endregion
}
