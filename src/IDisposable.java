package src;

/**
 * Created by OBI on 5.11.2014.
 */
public interface IDisposable {
    boolean dispose();
    boolean disposing();

}
