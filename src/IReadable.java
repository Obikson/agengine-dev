package src;

import org.jdom2.Element;

/**
 * Created by obert on 3. 11. 2014.
 */

public interface IReadable {
    void initFromNode(Element node);
    String getNodeName();
}
