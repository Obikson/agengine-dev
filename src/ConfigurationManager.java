package src;

/**
 * Created by OBI on 9.11.2014.
 */
public class ConfigurationManager extends EngineUnit<ConfigurationManager> implements IManager<Attribute> {

    Pool<Attribute> Items;

    @Override
    public ConfigurationManager memberwiseClone() {
        return null;
    }

    @Override
    public boolean dispose() {
        return false;
    }

    @Override
    public boolean disposing() {
        return false;
    }

    @Override
    public boolean preInit() {
        Items =   new Pool<Attribute>();
        return false;
    }

    @Override
    public boolean init() {
        return false;
    }

    @Override
    public boolean postInit() {
        return false;
    }

    @Override
    public Pool<Attribute> getItems() {
        return Items;
    }

    @Override
    public void setItems(Pool<Attribute> items) {
        Items = items;
    }

    @Override
    public void loadItems(String path, String filename) {
        Pool config = new Pool();
    }

    @Override
    public void saveItems(String path, String filename) {

    }

    @Override
    public String getNodeName() {
        return ConfigurationManager.class.getName();
    }
}
