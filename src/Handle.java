package src;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by obert on 7. 11. 2014.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Handle {
    String name() default "Default";
    int value() default -1;
}
