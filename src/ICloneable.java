package src;

/**
 * Created by obert on 3. 11. 2014.
 */
public interface ICloneable<T> {
    T memberwiseClone();
    T rawClone();
}
