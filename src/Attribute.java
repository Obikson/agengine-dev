package src;

import org.jdom2.Element;

/**
 * Created by obert on 3. 11. 2014.
 */
public class Attribute<T> extends EngineUnit<Attribute> {

    public Class<T> Type;

    public String Comment;

    private T Value;

    public Attribute()
    {}


    public Attribute(String name, Class<T> type) {
        setName(name);
        Type = type;
    }

    public Attribute(String name, Class<T> type, T value) {
        this(name, type);
        Value = value;
    }

    public Attribute(int id, String name, Class<T> type, T value) {
        this(name, type,value);
        setId(id);
    }

    public T getValue() {
        return Value;
    }

    public void setValue(T value) {
        Value = value;
    }

    @Override
    public void initFromNode(Element node) {
        super.initFromNode(node);

        Comment = node.getText();
        try {
            Type = (Class<T>) Class.forName(node.getAttributeValue("Type"));
            String tempAttrValue = node.getAttributeValue("Value");

            Value = Attribute.parse(tempAttrValue, Type);
        } catch (ClassNotFoundException exc) {
            System.out.println("Chyba -> spatny import hodnoty atributu (AttributeValue.java)");
        }
    }

    @Override
    public Element writeToNode() {
        Element result = super.writeToNode();

        if (Value != null)
            result.setAttribute("Value", Value.toString());


        result.setAttribute("Type", Type.getName());

        result.setText(Comment);

        return result;
    }

    @Override
    public String getNodeName() {
        return Attribute.class.getName();
    }


    @Override
    public Attribute memberwiseClone() {
        return new Attribute(getName(), Type, getValue());
    }

    @Override
    public boolean dispose() {
        return false;
    }

    @Override
    public boolean disposing() {
        return false;
    }

    @Override
    public boolean preInit() {
        return false;
    }

    @Override
    public boolean init() {
        return false;
    }

    @Override
    public boolean postInit() {
        return false;
    }

    public static <T>  T parse(String value, Class<T> toType){
        if (toType == Integer.class || toType == int.class) {
            return  (T) (Object) Integer.parseInt(value);
        } else if (toType == Double.class || toType == double.class) {
            return (T) (Object) Double.parseDouble(value);
        } else if (toType == Boolean.class || toType == boolean.class) {
            return (T) (Object) Boolean.parseBoolean(value);
        } else if (toType == Class.class) {
            try {
                return (T) Class.forName(value);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            return (T) (value);
        }
        return null;
    }
}
