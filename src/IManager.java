package src;

/**
 * Created by OBI on 9.11.2014.
 */
public interface IManager<T extends EngineUnit> {
    Pool<T> getItems();
    void setItems(Pool<T> items);
    void loadItems(String path, String filename);
    void saveItems(String path, String filename);
}
