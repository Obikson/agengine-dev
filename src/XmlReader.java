package src;


import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;

import java.io.File;

/**
 * Created by obert on 4. 11. 2014.
 */
public class XmlReader {
    public void parse(String absPath, IReadable objectToParse) {
        SAXBuilder builder = new SAXBuilder();
        File xmlFile = new File(absPath);
        try {

            Document document = builder.build(xmlFile);
            Element rootNode = document.getRootElement();
            objectToParse.initFromNode(rootNode);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
