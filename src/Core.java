package src;

import org.jdom2.Element;

/**
 * Created by OBI on 8.11.2014.
 */


public class Core extends EngineUnit<Core> {

    public Pool getConfiguration() {
        return Configuration;
    }

    public void setConfiguration(Pool configuration) {
        Configuration = configuration;
    }

    public TemplatesManager getTemplates() {
        return Templates;
    }

    public void setTemplates(TemplatesManager templates) {
        Templates = templates;
    }

    private Pool<Attribute> Configuration;

    private TemplatesManager Templates;

    public Core() {
    }

    @Override
    public Core memberwiseClone() {
        Core result = new Core();
        result.Configuration = Configuration.memberwiseClone();
        result.Templates = Templates.memberwiseClone();
        return result;
    }

    //region IDisposable
    @Override
    public boolean dispose() {
        return false;
    }

    @Override
    public boolean disposing() {
        return false;
    }
    //endregion

    //region IInititalizable
    @Override
    public boolean preInit() {
        Templates = new TemplatesManager();
        Templates.preInit();
        return false;
    }

    @Override
    public boolean init() {

        Attribute<String> attribute = Configuration.get("DefaultTemplates");

        Templates.loadItems(System.getProperty("user.dir"), attribute.getValue());

        return false;
    }

    @Override
    public boolean postInit() {
        return false;
    }
    //endregion

    @Override
    public void initFromNode(Element node) {
        super.initFromNode(node);

    }

    @Override
    public Element writeToNode() {
        Element root = super.writeToNode();
        root.addContent(Configuration.writeToNode());
        return root;
    }


    @Override
    public String getNodeName() {
        return Core.class.getName();
    }
}
