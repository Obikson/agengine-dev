package src;

import org.jdom2.Element;

import java.util.List;

/**
 * Created by OBI on 9.11.2014.
 */
public class TemplatesManager extends EngineUnit<TemplatesManager> implements IManager<Attribute<String>> {

    public Pool<Pool<Attribute<String>>> getConfigs() {
        return Configs;
    }

    private Pool<Pool<Attribute<String>>> Configs;

    public Pool<Pool<Attribute<Integer>>> getHandles() {
        return Handles;
    }

    private Pool<Pool<Attribute<Integer>>> Handles;

    private Pool<Attribute<String>> Templates;

    @Override
    public void initFromNode(Element node) {
        Factory factory = new Factory();

        List<Element> children = node.getChildren("Template");
        for (Element child : children) {
            Pool<Attribute<String>> configs = factory.readConfiguration(child);
            Pool<Attribute<Integer>> handles = factory.readHandles(child);
            Handles.add(handles, false);
            Configs.add(configs, false);
        }
    }

    @Override
    public String getNodeName() {
        return TemplatesManager.class.getName();
    }

    @Override
    public TemplatesManager memberwiseClone() {
        return null;
    }

    @Override
    public boolean dispose() {
        return false;
    }

    @Override
    public boolean disposing() {
        return false;
    }

    @Override
    public boolean preInit() {

        Configs = new Pool<Pool<Attribute<String>>>();
        Handles = new Pool<Pool<Attribute<Integer>>>();
        Templates = new Pool<Attribute<String>>();
        return false;
    }

    @Override
    public boolean init() {
        return false;
    }

    @Override
    public boolean postInit() {
        return false;
    }

    @Override
    public Pool getItems() {
        return Templates;
    }

    @Override
    public void setItems(Pool<Attribute<String>> items) {
        Templates = items;
    }

    @Override
    public void loadItems(String path, String filename) {
        XmlReader reader = new XmlReader();

        reader.parse(path + "\\" + filename, this);

    }

    @Override
    public void saveItems(String path, String filename) {

    }
}
