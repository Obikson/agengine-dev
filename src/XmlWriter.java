package src;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by obert on 3. 11. 2014.
 */
public class XmlWriter {

    XMLOutputter outfitter = new XMLOutputter();

    public void writeAndSave(String filename, IWritable node) throws IOException {
        Document document = new Document(node.writeToNode());

        outfitter.setFormat(Format.getPrettyFormat());
        outfitter.output(document, new FileWriter(filename));
    }

    public void writeAndSave(String filename, Element node) throws IOException {
        Document document = new Document(node);

        outfitter.setFormat(Format.getPrettyFormat());
        outfitter.output(document, new FileWriter(filename));
    }
}
