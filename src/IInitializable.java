package src;

/**
 * Created by OBI on 5.11.2014.
 */
public interface IInitializable  {

    boolean preInit();
    boolean init();
    boolean postInit();

}
