package src;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by OBI on 8.11.2014.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Configurate {
    String name() default "not set";
    String value() default "";
}
