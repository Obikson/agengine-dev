package src;

import org.jdom2.Element;

/**
 * Created by obert on 3. 11. 2014.
 */

public interface IWritable {
    Element writeToNode();
    String getNodeName();
}
