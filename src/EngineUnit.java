package src;

import org.jdom2.Element;

/**
 * Created by obert on 7. 11. 2014.
 */
public abstract class EngineUnit<T> implements IWritable, IReadable, ICloneable<T>, IDisposable, IInitializable {

    private int Id;
    private String Name;

    public int getId(){return Id;}
    public void setId(int value){Id = value;}

    public String getName(){return Name;}
    void setName(String value){Name = value;}

    public EngineUnit()
    {
        Id = -1;
        Name = "Not Set (DC)";

    }

    @Override
    public Element writeToNode(){
        Element result = new Element(getNodeName());
        result.setAttribute("Id", Integer.toString(getId()));
        result.setAttribute("Name", Name);
        return result;
    }

    @Override
    public void initFromNode(Element node){
        setId(Integer.parseInt(node.getAttributeValue("Id")));
        setName(node.getAttributeValue("Name"));
    }

    @Override
    public T rawClone()
    {
        EngineUnit<T> result = ((EngineUnit<T>)new Object());
        result.Id = getId();
        result.Name = getName();
        return (T)result;
    }
}
